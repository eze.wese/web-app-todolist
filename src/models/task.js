
const mongoose = require('mongoose');
const { Schema } = mongoose; //me permite deifinir el esquema de los datos.

const TaskSchema = new Schema ({ //se crea el modelo de esquema a utilizar
    title: { type: String, required: true },
    description: { type: String, required: true }
});

module.exports = mongoose.model('Task', TaskSchema); // para reutlizar el modelo de datos en la app, utilizo mongoose y lo exporto