
const mongoose = require('mongoose');

const URL = 'mongodb://localhost/tasks';

mongoose.connect(URL, { useNewUrlParser: true, useFindAndModify: false })
		.then(db => console.log('db connected'))
		.catch(err => console.error(err));


module.exports = mongoose;