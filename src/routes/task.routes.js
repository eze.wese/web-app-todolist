
const express = require('express');
const router = express.Router();

const Task = require('../models/task'); //almaceno el modelo en una constante.

router.get('/', async (req, res) => {
    const tasks  = await Task.find(); // el await lo que me dice es: esta accion va a tomar algo de tiempo, luego de que termine, lo que devuelva se almacene en una constante.
    res.json(tasks);

});

router.get('/:id', async (req, res) => {
    const task = await Task.findById(req.params.id);
    res.json(task);
});

router.post('/', async (req, res) => {
    const { title, description } = req.body;
    const task = new Task({title, description});
    await task.save();
    res.json({status: 'Task saved'});
});

router.put('/:id', async (req, res) => {
    const {title, description} = req.body;
    const newTask = {title, description};
    await Task.findByIdAndUpdate(req.params.id, newTask);
    res.json({status: 'Task updated'});
});

router.delete('/:id', async (req, res) => {
    await Task.findByIdAndRemove(req.params.id);
    res.json({status: 'Task deleted'});
});

module.exports = router;