
const express = require('express');
const morgan = require('morgan');
const path = require('path'); //utilizo el modulo path porque no se en que servidor (linux o windows) va a correr la app, por eso la sintaxis puede cambiar.
const { mongoose } = require('./database');
const app = express();


//settings
app.set('port', process.env.PORT || 3000); //esto es para hacer generico el puerto en el se va a correr la app. Le asigna uno manual 3000 y otro por defecto del servidor


//middlewares (funciones que se ejecutan antes de que lleguen a la ruta).
app.use(morgan('dev')); 
app.use(express.json());


//routes
app.use('/api/task', require('./routes/task.routes'));

//static files
app.use(express.static(path.join(__dirname, 'public'))); //le digo al navegador que archivo html leer.


//starting server
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});